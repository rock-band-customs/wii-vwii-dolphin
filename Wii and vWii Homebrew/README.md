# Wii/vWii Homebrew

Put these on your SD card or USB drive, in the `apps` folder where the Homebrew Channel can see it.

> Right now, only DarkMatterCore's Xyzzy Mod is available, but there will be more in the future!

# Other Homebrew

These are not provided by this repo, but may be needed in order to use customs and re-add DLC.

* **WiiXplorer:** It's a full-blown graphical file manager for the Wii, which allows cut-and-paste, and includes a few built-in file editors. [Download from Sourceforge.](https://sourceforge.net/projects/wiixplorer/)
* **Some YAWMM (Yet Another WAD Manager Mod) Mod:** A WAD manager for the Homebrew Channel. It can install/uninstall WADs on your Wii. [Download from GitHub.](https://github.com/FIX94/Some-YAWMM-Mod)
* **Waninkoko's custom IOSes:** Follow the cIOS steps on the [Complete Softmod Guide](https://sites.google.com/site/completesg/backup-launchers/installation).
* **CleanRip:** If you need to rip a Rock Band disc for Dolphin and have a Wii or Wii U handy, install CleanRip from [GitHub](https://github.com/emukidid/cleanrip).
* **WiiFlow:** A Mac-like USB Loader. Download from [GitHub](https://github.com/Fledge68/WiiFlow_Lite/releases).
* **USB Loader GX:** Recommended for the most customization. Download from [SourceForge](https://sourceforge.net/projects/usbloadergx/).
