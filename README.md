# :guitar: THE ROCK BAND CUSTOMS PROJECT 

This GitLab repository is part of the **Rock Band Customs Project** founded by Harvey Houston (HarvHouHacker). It is a collection of software designed to be used for the consoles mentioned in the project title. For info about proper usage, copyright, and other information, please see the Terms and Conditions before using ANY of the software provided here.

# :video_game: ROCK BAND CUSTOMS FOR WII - VWII - DOLPHIN

These tools will allow you to add more songs despite the closure of the Wii Shop.

## :exclamation: A Word of Warning

Any modifications may brick your Wii or Wii U. This means that it may make your Wii or Wii U unusable. I strongly recommend backing up saves and WiiWare to an SD, and for those who may be a bit ignorant (it's okay! We all were once), the NAND as well. If you do not want to mod, you could try using Dolphin instead - if you don't feel comfortable about emulators, stop reading!

# :book: PLEASE READ THE WIKI!

Newcomers to Rock Band hacking may find this software confusing if they don't know how to use it. I recommend a careful and thorough reading of the **Basic Guide to Adding Customs** section of the [Rock Band Customs GitLab Wiki](https://gitlab.com/HarvHouHacker/rock-band-customs/-/wikis/home) to get you started.

# :file_folder: REPO CONTENTS

For PC and Homebrew Channel software specifically for Rock Band, download the entire `master` branch, either by using `git`, or by downloading a compressed package (ZIP, TAR, or whatever) directly from here on GitLab.

## :desktop: For Windows

A minimum version of Windows 7 is recommended to run this software, although this software can be used on versions as low as XP 64-bit. For Linux users, WINE **may** work. Anyone who tests these in WINE is welcome to report their findings, and create new issues if they run across problems.

* **Xorloser's ArkTools:** Tools which extract and modify archives on Rock Band game discs. **Do not share extracted archives!**
* **Album Make:** This software will make album covers for you, in `.png_wii` format. 
* **MahoppianGoon's Rock Band DLC Tools:**  Made mainly for Rock Band customs on the Xbox, it also has some very useful tools for the Wii versions. The tools include:
    - *OGG2MOGG:* This will convert multitrack OGG audio files into Rock Band compatible files with the MOGG extension.
    - *Songs.dta Editor:* This will aid in editing DTA files so that they can be read by Rock Band titles.
    - The other tools are mainly for Xbox users, but you are welcome to mess aroud with 'em anyway!'

## :video_game: For Wii/vWii

* **xyzzy:** A small program which dumps console keys. To use, inject the entire folder into your SD Card's `apps` folder.

## :package: DLC Packers

These are in two branches - `DLC_Packers_NTSC` and `DLC_Packers_PAL`. If using Git type `git checkout DLC_Packers_REGION` to switch to the ones you want. If downloading via ZIP or other archive, browse and download the right branch and extract to a project folder.

The WADs in `Ticket-WADs` are small updates which would have allowed you to download content from the Rock Band Music Store for free. Now, however, they're being used to add customs.

## :link: Links to Additional Software

These are links to other software that will be very useful for making Rock Band custom DLC.

* **WiiXplorer:** Much more user-friendly than FS Toolbox. It's a full-blown graphical file manager for the Wii, which allows cut-and-paste, and includes a few built-in file editors. [Download from Sourceforge.](https://sourceforge.net/projects/wiixplorer/)
* **ShowMiiWads:** A WAD manager for Windows. It can create the `common-key` file that you need for packing DLC. [Download from Sourceforge.](https://sourceforge.net/projects/showmiiwads/)
* **Some YAWMM (Yet Another WAD Manager Mod) Mod:** A WAD manager for the Homebrew Channel. It can install/uninstall WADs on your Wii. [Download from GitHub.](https://github.com/FIX94/Some-YAWMM-Mod)

*More software is mentioned in the wiki.*

# :thumbsup: SPECIAL THANKS

* **pksage** - The guy who pretty much conceived Rock Band customs to begin with, he created Customs Creators (which is now Rhythm Gaming World). He is, of course, on [Rhythm Gaming World](https://rhythmgamingworld.com/members/pksage/).
* **StackOverflow0x/Koetsu** - This guy is the one who inspired me to go all-out on hacking Rock Band 3. You can check out his profiles on [Rhythm Gaming World (as StackOverflow0x)](https://rhythmgamingworld.com/members/stackoverflow0x/) and [ScoreHero (as Koetsu)](https://rockband.scorehero.com/scores.php?user=484319&diff=). My guide is based off of his guides, but his are a little different because they also explain how to use Xbox 360 files more in-depth than mine does.
* **Alternity** - He helped me understand how to use copyright material responsibly, and his suggestions on improving RBC Discord made it all the more manageable. He submitted various Rock Band tools to Discord, which have helped a great deal. He is on [Rhythm Gaming World](https://rhythmgamingworld.com/members/alternity/).
* **Farottone** - One of the administrators of Rhythm Gaming World, he helped me with understanding about copyrighted content, and is kind of like a Rock Band sensei to me. You can see him on [Rhythm Gaming World](https://rhythmgamingworld.com/members/farottone/).
* **Atruejedi** - He broke down the guide by StackO, explaining more about the whys as well as the the what-not-to-dos. His profile is on [Rhythm Gaming World](https://rhythmgamingworld.com/members/atruejedi/).
* **TrashRBPlayer** - Another person who really helped me out on RBC Discord, he suggested expanding the Rock Band Customs project to more than just for Wii/vWii. Find him on [Rhythm Gaming World](https://rhythmgamingworld.com/members/trashrbplayer).
* **MahoppianGoon** - His software and guides are mainly for Xbox 360, but his OGG2MOGG and songs.dta Editor Tools help a lot for Wii customs! Visit his site on [Google Sites](https://sites.google.com/site/mahoppiangoon/home).
* **Xorloser** - He created the Arktools suite which can extract ARK packages found in Rock Band discs, as well as convert songs from Gutar Hero to be used in Rock Band, among other things which I have yet to figure out. Find his blog on [WordPress](http://www.xorloser.com).
* **Larsen Vallecillo (Larsenv)** - A great programmer and lover of Wii homebrew, he helped create a WAD2BIN program which creates SD-card-capable BIN files out of Wii WAD archives. You can find him on [his own website](https://larsenv.xyz) as well as on [GitHub](https://github.com/larsenv) and [GBAtemp](https://gbatemp.net/members/larsenv.335657/).
* **Pablo Curiel (DarkMatterCore)** - He developed and released Xyzzy (unofficial port), Yet Another BlueDump Mod, and the WAD2BIN program, as well as offer insights on what it's like in Venezuela. He's on [GitHub](https://github.com/DarkMatterCore) and [GBAtemp](https://gbatemp.net/members/darkmattercore.180929/).
* **Ethan Gordon (Miramur)** - With an interest in robotics and programming, he jumped right into the project, and has helped enhance the wiki as well as filling in for me on Discord. Find him on [his website](https://ethankgordon.com), [GitLab](https://gitlab.com/ekgordon), and [GitHub](https://github.com/egordon).
