# Make Your Own Album Covers!

You can use this simple little program to create album covers which can be used for custom songs. Find an album cover or create your own and save it as a 128x128 PNG image. Make sure to match the condensed song name format (a few examples already exist here to give you an idea). Then, put it in the `input` folder and run `AlbumMake.bat`. This will execute `PNGWiiCreator.exe` and convert all `songname.png` images from `input` to `songname_keep.png_wii` in the `png_wii` folder.
