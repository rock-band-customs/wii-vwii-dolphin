# Parranoyed's DLC Recrypter

*Also known as DLC packer/unpacker on the [A Parannoyed Wii](https://sites.google.com/site/parannoyedwii/projects/dlc-packer-unpacker) site.*

> This build is version 0.1-test3, and may not work with all types of DLC.

# About

Decrypts official IOS DLC and encrypts for use with certain homebrew IOS's. Technically speaking, these tools will work with any game that uses this format.

**THIS PROGRAM IS NOT INTENDED FOR USE IN ANY ILLEGAL ACTIVITIES.**

**Parannoyed credits:**
>>>
Credit due...
  segher for his incredible wii.git
  booto for his bv
>>>

# How does it work?

it requires your Console ID and PRNG seed saved in binary format as "id" and "pack-key".  They should be in the same directory as the unpacker and packer. **THE INCLUDED ID AND PACK-KEY ARE EMPTY -- YOU MUST REPLACE THEM WITH YOUR OWN.**

The unpacker will create a directory named for the title id of the save files and saves the tmd and .app file in that directory.

The packer expects the .app file to be located inside the same directory (title id of save file) the original .bin was unpacked to.  It will save the new .bin file in that same directory.

# Current Tests

* **Rock Band 2** - Works well with RawkSD, but does not work at all with newer methods. RB2 may need a different encryption method.
* **Rock Band 3** - Not needed for sZAE-sZFE, as they load well with no PRNG seed, but sZJE-sZME *needs* a valid PRNG. As of this writing, this build allows songs in those generations to work flawlessly using current methods.
* **The Beatles: Rock Band** - Not fully tested, but may not be needed as loading PRNG-less APPS and BINs seem to work just fine.
* **Green Day: Rock Band** - Not tested, but we suspect that songs built for this title may also not be required to be signed with the PRNG seed.

# Using the Recrypter

Scripts:
  recrypt-all (recrypts every .bin in the directory)
   
  recrypt (recrypts a single .bin)
    example:  recrypt 000.bin

Unpacker/packer:
  dlcunpack <bin file>
    example:  dlcunpack 002.bin

  dlcpack <tmd> <index> <game_title_id>
    example:  dlcpack 0001000563524241\tmd 002 535a4145
      (tmd from the unpacked file)
      (002 is the file you want to create, ie. 002.bin)
      (535a4145 is the title id for Rock Band 2)

# About the Keys

Use DarkMatterCore's Xyzzy Mod (included in the GitLab repo) to dump your keys. Open `id` in a hex editor and type in your Console ID.  Do the same for `pack-key`, using your PRNG seed.

> **IMPORTANT:** The `id` file size should be 4 bytes and `pack-key` should be 16 bytes.

*This version of Parannoyed's DLC Recrypter was last updated July 20th, 2009.*
