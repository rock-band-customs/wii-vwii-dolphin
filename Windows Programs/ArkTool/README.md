# Ark/Hdr Access Codebase v7.0  -  Created by xorloser

### Before using... Read This Piracy Warning

This program is designed to break .ark encryption and give access
to copyrighted content. By using this software, you promise not to
share or sell any of the files within such archives. The developers
and distributors of this software will not be held responsible for
the end user misusing copyrighted content.

This software is free to use and free to distribute; you are only
bound by the restrictions of the archives you use it on.

### Credits

*Coded by xorloser*  -  www.xorloser.com

Feel free to use this sourcecode in your own projects, but please
credit the use of this codebase when doing so.

If you notice any bugs or have any suggestions, please mail them to:
me@xorloser.com.